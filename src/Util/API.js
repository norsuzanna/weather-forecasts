import axios from "axios";
import { MESSAGE_403, MESSAGE_404 } from "../ErrorMessage";

let API_ROOT = "https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/";

export const get = async endpoint => {
  try {
    const response = await axios.get(API_ROOT + endpoint, {
      headers: { 'Access-Control-Allow-Origin': '*' }
    });

    return response;
  } catch (e) {
    if (e.response.status === 400) {
      return e.response;
    }
    if (e.response.status === 403) {
      return MESSAGE_403;
    }
    if (e.response.status === 404) {
      return MESSAGE_404;
    } else return e.response.data.error;
  }
};
