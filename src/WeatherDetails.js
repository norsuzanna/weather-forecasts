import React from "react";
import "./App.css";
import { List, Card } from "antd";
import { TEXT_SIX_DAYS_FORECAST } from "./Text";
import CustomizedButton from "./Components/CustomizedButton";

function WeatherDetails(props) {
  return (
    <span>
      {props.weatherDetails.length > 0 && (
        <div>
          <div className="App-close-button-container">
            <CustomizedButton
              text={<i className="far fa-times-circle" />}
              onClick={props.closeWeatherDetails}
              className="App-close-button"
            />
          </div>
          <p>{TEXT_SIX_DAYS_FORECAST}</p>
          <List
            className="App-forecast-details-list"
            grid={{
              gutter: 16,
              xs: 1,
              sm: 2,
              md: 4,
              lg: 4,
              xl: 6,
              xxl: 3
            }}
            dataSource={props.weatherDetails}
            renderItem={item => (
              <List.Item>
                <Card title={item.applicable_date}>
                  <div>
                    <img src={item.weather_state_icon} alt="weather_icon" />
                    <div className="App-weather-details-container">
                      <div className="App-max-temp">
                        <b>{item.max_temp.toFixed(0)}°</b>
                      </div>
                      <div className="App-min-temp">
                        <b>{item.min_temp.toFixed(0)}°</b>
                      </div>
                    </div>
                    <div>Humidity: {item.humidity}%</div>
                    <div>Wind: {item.wind_speed.toFixed(0)} km/h</div>
                  </div>
                </Card>
              </List.Item>
            )}
          />
        </div>
      )}
    </span>
  );
}

export default WeatherDetails;
