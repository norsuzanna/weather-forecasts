import React from "react";
import "./App.css";
import { Form, List, Spin } from "antd";
import { Formik } from "formik";
import {
  TEXT_ENTER_CITY_OR_PLACE,
  TEXT_NEW_LOCATION,
  TEXT_ENTER
} from "./Text";
import SearchInput from "./Components/SearchInput";
import CustomizedButton from "./Components/CustomizedButton";
import TextInput from "./Components/TextInput";
import InfiniteScroll from "react-infinite-scroller";

const formik = {
  initialValues: {
    newLocation: ""
  }
};

function Home(props) {
  return (
    <Formik
      {...formik}
      onSubmit={props.addWeather}
      render={form => (
        <Form onSubmit={form.handleSubmit} className="login-form">
          <SearchInput
            {...form}
            name="location"
            placeholder={TEXT_ENTER_CITY_OR_PLACE}
            isEnterButtonAvailable={true}
            className="App-search-input"
            onSearch={props.submitSearch}
            onChange={event => {
              form.setFieldValue("location", event.target.value);
              form.setFieldTouched("location", true, false);
              props.submitSearch(event.target.value);
            }}
          />
          {props.weathers.length > 0 && (
            <div className="demo-infinite-container">
              <InfiniteScroll
                initialLoad={false}
                pageStart={0}
                loadMore={props.handleInfiniteOnLoad}
                hasMore={!props.loading && props.hasMore}
                useWindow={false}
              >
                <List
                  className="App-search-input"
                  itemLayout="horizontal"
                  dataSource={props.weathers}
                  footer={
                    <div className="App-add-new-location">
                      <TextInput
                        {...form}
                        name="newLocation"
                        placeholder={TEXT_NEW_LOCATION}
                        type="text"
                        className="App-text-input"
                      />
                      <CustomizedButton
                        text={TEXT_ENTER}
                        className="App-add-button"
                        htmlType="submit"
                      />
                    </div>
                  }
                  renderItem={weather => (
                    <List.Item
                      actions={[
                        <CustomizedButton
                          text={<i className="fas fa-trash-alt" />}
                          onClick={() => props.deleteWeather(weather)}
                          className="App-list-button"
                        />
                      ]}
                      className="App-list-button"
                    >
                      <List.Item.Meta
                        title={weather.title}
                        className="App-title"
                        onClick={() => props.getWeatherDetail(weather)}
                      />
                    </List.Item>
                  )}
                >
                  {props.loading && props.hasMore && (
                    <div className="demo-loading-container">
                      <Spin />
                    </div>
                  )}
                </List>
              </InfiniteScroll>
            </div>
          )}
        </Form>
      )}
    />
  );
}

export default Home;
