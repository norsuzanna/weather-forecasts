export const TEXT_PLACE = "Place";
export const TEXT_CITY = "City";
export const TEXT_OR = "or";
export const TEXT_IS = "is";
export const TEXT_ENTER = "Enter";
export const TEXT_ENTER_CITY_OR_PLACE =
  `${TEXT_ENTER} ` +
  `${TEXT_CITY} `.toLowerCase() +
  `${TEXT_OR} ` +
  `${TEXT_PLACE}`.toLowerCase();
export const TEXT_REQUIRED = "Required";
export const TEXT_IS_REQUIRED =
  `${TEXT_IS} ` + `${TEXT_REQUIRED}`.toLowerCase();
export const TEXT_WELCOME =
  "Weather forecasts for thousands of locations around the world";
export const TEXT_NEW_LOCATION = "New location";
export const TEXT_SIX_DAYS_FORECAST = "Six Days Forecast";
export const PLEASE_TRY_AGAIN = "Please try again";
