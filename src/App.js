import React, { useState } from "react";
import "./App.css";
import { Spin } from "antd";
import { TEXT_WELCOME } from "./Text";
import { get } from "./Util/API";
import moment from "moment";
import WeatherDetails from "./WeatherDetails";
import Home from "./Home";

const capitalize = string => {
  if (typeof string !== "string") return "";
  return string.charAt(0).toUpperCase() + string.slice(1);
};

let emptyArray = [];

function App() {
  const [weathers, setWeather] = useState([]);
  const [weatherDetails, setWeatherDetails] = useState([]);
  const [loading, setLoading] = useState(false);
  const [listLoading, setListLoading] = useState(false);
  const [hasMore, setHasMore] = useState(true);
  const [hasInfo, setHasInfo] = useState(false);

  async function submitSearch(value) {
    setListLoading(true);

    if (value) {
      const response = await get("search/?query=" + value);

      if (response.status === 200) {
        let sortedWeathers = response.data.sort(
          (a, b) => a.title.length - b.title.length
        );

        setWeather(sortedWeathers);
      }
    } else {
      setWeather(emptyArray);
    }

    setListLoading(false);
  }

  function handleInfiniteOnLoad() {
    setLoading(true);

    if (weathers.length > 0) {
      setHasMore(false);
      setLoading(false);
      return;
    }
  }

  async function getWeatherDetail(weather) {
    setHasInfo(true);
    setListLoading(true);

    if (weather.woeid) {
      const response = await get(weather.woeid);

      if (response.status === 200) {
        let consolidated_weather = response.data.consolidated_weather;

        for (let index = 0; index < consolidated_weather.length; index++) {
          consolidated_weather[index].applicable_date = moment(
            consolidated_weather[index].applicable_date
          ).format("ddd, DD/MM");
          consolidated_weather[index].weather_state_icon =
            "https://www.metaweather.com/static/img/weather/" +
            consolidated_weather[index].weather_state_abbr +
            ".svg";
        }

        setWeatherDetails(consolidated_weather);
      }
    } else {
      setHasInfo(false);
    }

    setListLoading(false);
  }

  function closeWeatherDetails() {
    setHasInfo(false);
    setWeatherDetails(emptyArray);
  }

  function deleteWeather(weather) {
    var remainingWeather = weathers.filter(function(item) {
      return item.title !== weather.title;
    });

    setWeather(remainingWeather);
  }

  function addWeather(values) {
    var sameName = weathers.find(function(item) {
      return item.title.toLowerCase() === values.newLocation.toLowerCase();
    });

    if (!sameName && values.newLocation.length > 0) {
      setWeather([...weathers, { title: capitalize(values.newLocation) }]);
    }
  }

  return (
    <Spin spinning={listLoading}>
      <div className="App">
        <header className="App-header">
          {hasInfo ? null : <p className="App-link">{TEXT_WELCOME}</p>}
          {hasInfo ? (
            <WeatherDetails
              weatherDetails={weatherDetails}
              closeWeatherDetails={closeWeatherDetails}
            />
          ) : (
            <Home
              addWeather={addWeather}
              submitSearch={submitSearch}
              weathers={weathers}
              weatherDetails={weatherDetails}
              handleInfiniteOnLoad={handleInfiniteOnLoad}
              loading={loading}
              hasMore={hasMore}
              deleteWeather={deleteWeather}
              getWeatherDetail={getWeatherDetail}
            />
          )}
        </header>
      </div>
    </Spin>
  );
}

export default App;
