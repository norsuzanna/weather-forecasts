import React from "react";
import { Form, Input } from "antd";

const TextInput = ({
  values,
  errors,
  setFieldValue,
  setFieldTouched,
  name,
  placeholder,
  type,
  prefix,
  label,
  max,
  onChange,
  addonAfter,
  addonBefore,
  size,
  disabled,
  className,
  touched,
  style
}) => (
  <Form.Item
    hasFeedback={addonAfter || addonBefore ? null : !!errors[name]}
    validateStatus={disabled ? null : touched[name] && errors[name] && "error"}
    help={disabled ? null : touched[name] ? errors[name] : ""}
    label={label}
    className={className}
    style={style}
  >
    <Input
      disabled={disabled}
      prefix={prefix}
      addonAfter={addonAfter}
      addonBefore={addonBefore}
      placeholder={placeholder}
      value={values[name]}
      onChange={
        onChange
          ? onChange
          : event => {
              if (event.target.value.length === max + 1) {
                return;
              }
              setFieldValue(name, event.target.value);
              setFieldTouched(name, true, false);
            }
      }
      type={type}
      size={size}
    />
  </Form.Item>
);

export default TextInput;
