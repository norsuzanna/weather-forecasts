import React from "react";
import { Form, Input } from "antd";

const { Search } = Input;

const SearchInput = ({
  values,
  errors,
  setFieldValue,
  setFieldTouched,
  name,
  placeholder,
  label,
  className,
  touched,
  itemStyle,
  isEnterButtonAvailable,
  searchStyle,
  onSearch,
  onChange
}) => (
  <Form.Item
    hasFeedback={!!errors[name]}
    validateStatus={touched[name] && errors[name] && "error"}
    help={touched[name] ? errors[name] : ""}
    label={label}
    className={className}
    style={itemStyle}
  >
    <Search
      value={values[name]}
      placeholder={placeholder}
      onSearch={onSearch}
      onChange={
        onChange
          ? onChange
          : event => {
              setFieldValue(name, event.target.value);
              setFieldTouched(name, true, false);
            }
      }
      style={searchStyle}
      enterButton={isEnterButtonAvailable}
    />
  </Form.Item>
);

export default SearchInput;
